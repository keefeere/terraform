$token=Get-Content c:\Users\keefeere\OneDrive\DevOps\Vagrant\tmp\gitlab-root-personal-access-token.txt 
$uname="keefeere"
$userpass=Get-Content c:\Users\keefeere\OneDrive\DevOps\Terraform\GitLabSelf\pass.txt

Set-Item -Path env:TF_VAR_gitlab_token -Value $token
Set-Item -Path env:TF_VAR_username -Value $uname
Set-Item -Path env:TF_VAR_userpass -Value $userpass

terraform validate
terraform plan
terraform apply
Set-Item -Path env:TF_VAR_gitlab_token -Value ""
Set-Item -Path env:TF_VAR_username -Value ""
Set-Item -Path env:TF_VAR_userpass -Value ""
