
variable "base_url" {
  type = string
  default = "http://gitlabvagrant/api/v4/"
}

variable gitlab_token {}
variable username {}
variable userpass {}

terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
  required_version = ">= 0.13"
}

provider "gitlab" {
    base_url = var.base_url
    token = var.gitlab_token
}


resource "gitlab_user" "usertf" {
  name             = "Chechulin Sergey"
  username         = var.username
  password         = var.userpass
  email            = "keefeere@ukr.net"
  is_admin         = true
  can_create_group = true
  is_external      = false
  reset_password   = false
}


resource "gitlab_group" "grouptf" {
  name        = "WA_group"
  path        = "wagroup"
  description = "Group for Web Academy"
  visibility_level = "public"
}


resource "gitlab_group_membership" "test" {
  group_id     = gitlab_group.grouptf.id
  user_id      = gitlab_user.usertf.id
  access_level = "owner"
}


// Create a project in the example group
resource "gitlab_project" "projtf" {
  name         = "Terraform"
  description  = "Terraform GitLab setup"
  namespace_id = gitlab_group.grouptf.id
  visibility_level = "public"
}




# resource "gitlab_project" "new-repo" {
#   name         = "new-repo"
#   description  = "New repo"
# #  namespace_id = var.namespace_id
# }